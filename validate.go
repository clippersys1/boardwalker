package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func validateAndConvertToIntSlice(s []string) ([]int, []int, []int) {
	// grabbing the most low-hanging fruit of them all, comma typo
	if strings.Contains(strings.Join(s, ","), ",,") {
		fmt.Println(usage + "\ninput typo, check your commas...")
		os.Exit(1)
	}
	// create slices for the differents tasks:
	// creating the board, initial position of the marker
	// and the moves
	b := s[:2]
	i := s[2:4]
	m := s[4:]
	// validate and get the slices separately
	board := validateAndConvertBoard(b)
	initpos := validateAndConvertInitPos(board, i)
	moves := validateAndConvertMoves(m)
	//	return board, initpos, moves
	return board, initpos, moves
}

func validateAndConvertBoard(b []string) []int {
	test := regexp.MustCompile(`^[0-9]*$`)
	if !test.MatchString(strings.Join(b, "")) {
		fmt.Println(usage + "\nBad board coordinates format: " + b[0] + " " + b[1] + "\n")
		os.Exit(1)
	}
	b0, _ := strconv.Atoi(b[0])
	b1, _ := strconv.Atoi(b[1])
	return []int{b0, b1}
}

func validateAndConvertInitPos(b []int, i []string) []int {
	test := regexp.MustCompile(`^[0-9]*$`)
	if !test.MatchString(strings.Join(i, "")) {
		fmt.Println(usage + "\nBad marker coordinates format: " + i[0] + " " + i[1] + "\n")
		os.Exit(1)
	}
	i0, _ := strconv.Atoi(i[0])
	i1, _ := strconv.Atoi(i[1])
	if (i0 > b[0]) || (i1 > b[1]) {
		fmt.Println(usage + "\nMarker outside the board: " + i[0] + " " + i[1] + "\n")
		os.Exit(1)
	}
	return []int{i0, i1}
}

func validateAndConvertMoves(m []string) []int {
	// grab the low-hanging fruit first. If the list commands
	// does not end with 0, bail out
	m0 := m[len(m)-1]
	if m0 != "0" {
		fmt.Println(usage + "\nMoves must end with 0 = stop simulation: " + m0 + "\n")
		os.Exit(1)
	}
	// drop the last element, the 0, to faciliate testing the rest
	// of the commands, which should all be 1-4 digits
	m = m[:len(m)-1]
	test := regexp.MustCompile(`^[1-4]*$`)
	if !test.MatchString(strings.Join(m, "")) {
		fmt.Println(usage + "\nBad move format: can only be digits 1-4,\nending with 0 for stopping\n")
		os.Exit(1)
	}
	// append the zero again
	m = append(m, "0")
	// convert []string slice to []int slice
	var mInt []int
	var tmp int
	for i := 0; i < len(m); i++ {
		tmp, _ = strconv.Atoi(m[i])
		// in the unlikely event that someone tried to sneak in a move larger
		// than one digit using only digits 1-4, like 11, 12, 33, 42 etc
		if tmp > 4 {
			fmt.Println(usage + "\nBad move format: can only be single digits 1-4,\n")
			os.Exit(1)
		}
		mInt = append(mInt, tmp)
	}
	return mInt
}
