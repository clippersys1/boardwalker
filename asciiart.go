package main

import (
	"fmt"
	"github.com/gookit/color"
	"os"
	"os/exec"
	"runtime"
	"time"
)

func doAsciiArt(b Board, p Position) {
	// clear screen
	cmd := exec.Command("clear")
	if runtime.GOOS == "windows" {
		// untested on windows
		cmd = exec.Command("cls")
	}
	cmd.Stdout = os.Stdout
	cmd.Run()
	// nice header
	fmt.Println("\nB O A R D W A L K E R")
	fmt.Println("        by K\n")
	// top line with x values in yellow
	fmt.Print("      ")
	for t := 0; t < b.x; t++ {
		color.Print(color.Yellow.Sprint(t), "   ")
	}
	fmt.Println()
	// the board and the moves..
	for y := 0; y < b.y; y++ {
		color.Print(" ", color.Yellow.Sprint(y), "  ")
		for x := 0; x < b.x; x++ {
			// top line
			if (p.x == x) && (p.y == y) {
				fmt.Print("+ ")
				color.Print(color.Red.Sprint("O"))
				fmt.Print(" ")
			} else {
				fmt.Print("+   ")
			}
		}
		fmt.Println("+")
	}
	fmt.Println()
	time.Sleep(400 * time.Millisecond)
}
