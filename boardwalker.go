/*
	Boardwalker
	Kistoffer Hell, March 8, 2021
	info@clippersys.eu

	Program written for show, more comments
	than might have been expected

*/
package main

import (
	"fmt"
	"os"
	"reflect"
	"strings"
)

var usage string = `
Usage:    

go run boardwalker.go <serie of digits>

Ex: 8,7,5,6,1,2,3,4,0

Where: 8 & 7 are x and y giving the size of the 
rectangular shaped board.
The two next digits: 5 & 6 indicate the starting
coordinate of the marker (x,y).
The reminder of the serie, 1,2,3,4,0, are the moves.
1 = forward. 2 = backward. 3 = turn right 90 deg (no move). 
4 = turn left 90 deg (no move).
0 = stop simulation.
The initial direction of the marker is up/North. 

Verbose (debug mode):

go run boardwalker.go <serie of digits> --verbose
`

// allow for printout statements
// var debug bool = true
var debug bool = false

// show board and movements as ascii
var asciiart bool = false

type Board struct {
	x int
	y int
}

type Position struct {
	x int
	y int
}

type Moves []int

func main() {
	// no arg call: show usage
	if len(os.Args) < 2 {
		fmt.Println(usage)
	} else {

		if len(os.Args) == 3 && os.Args[2] == "--verbose" {
			debug = true
		}

		if len(os.Args) == 3 && os.Args[2] == "--asciiart" {
			asciiart = true
		}

		// read input from stdin into a []string
		s := strings.Split(os.Args[1], ",")

		// assume s needs to contain at least six slots:
		// 2 for the board, 2 for the init pos of the object/
		// marker and 2 for the moves (1 move and 0 for stop)

		if len(s) < 6 {
			fmt.Println(usage)
		} else {

			// validate and convert to []int slices
			b, p, m := validateAndConvertToIntSlice(s)
			if debug {
				fmt.Println("board: ", b)
				fmt.Println("marker pos: ", p, reflect.TypeOf(p))
				fmt.Println("moves: ", m)
			}
			// validation done, all good - let's play...

			var board Board
			board.x = b[0]
			board.y = b[1]

			var pos Position
			pos.x = p[0]
			pos.y = p[1]

			var moves Moves
			moves = m

			play(board, pos, moves)

		}
	}
}

//func play(board []int, pos []int, moves []int) {
func play(board Board, pos Position, moves Moves) {

	const GAME_OVER = 0
	const MOVE_FORWARD = 1
	const MOVE_BACKWARD = 2
	const TURN_LEFT = 3
	const TURN_RIGHT = 4

	// degrees instead of compass directions.
	// Perhaps less of a lock-in, in case the app is
	// rewritten to handle polar form notation.
	// Also makes the code a little less cluttered
	// direction
	dik := 0
	// counting the moves made. Not in the spec
	// but a nice feature when running the app
	// in --verbose mode
	counter := 0
	// holds the current move
	mov := 0
	for n := 0; n < len(moves); n++ {
		mov = moves[n]
		if mov == GAME_OVER {
			// game over
			fmt.Println(pos)
			return
		} else if (mov != TURN_LEFT) && (mov != TURN_RIGHT) {
			// make a move
			switch dik {
			case 0:
				if mov == MOVE_FORWARD {
					pos.y--
				} else {
					pos.y++
				}
			case 90:
				if mov == MOVE_FORWARD {
					pos.x++
				} else {
					pos.x--
				}
			case 180:
				if mov == MOVE_FORWARD {
					pos.y++
				} else {
					pos.y--
				}
			case 270:
				if mov == MOVE_FORWARD {
					pos.x--
				} else {
					pos.x++
				}
			}
			counter++
			// check to see if marker is still on the board
			if (pos.x > board.x-1) || (pos.y > board.y-1) || (pos.x < 0) || (pos.y < 0) {
				fmt.Println([]int{-1, -1})
				if debug {
					fmt.Println("\nMarker fell off the board after ", counter, "moves, of ", len(moves)-1, " possible moves.\nThe board: ", board, " marker: ", pos)
				}
				return // quit silently
			}
		} else {
			// no move, just redirection
			if mov == TURN_RIGHT {
				dik = dik + 90
				if dik >= 360 {
					dik = 0
				}
			}
			if mov == TURN_LEFT {
				dik = dik - 90
				if dik < 0 {
					dik = dik + 360
				}
			}
		}
		if debug {
			fmt.Println("move: ", mov, " pos: ", pos, " direction (deg): ", dik)
		}
		if asciiart {
			doAsciiArt(board, pos)
		}
	}
}
