# boardwalker

A golang command line program which simulates an object moving around on a 2D rectangular board.

## how to run

```
go run boardwalker.go 4,4,2,2,1,4,1,3,2,2,4,1,0
```

The first two digits: 4,4 - is the x and y values of the board, in this case a board with 4 times 4 slots (16 slots).

The next two digits: 2,2 - is the initial location of the object on the board.

The remaining string of digits are the moves undertaken by the object:

<ol>
<li>move forward</li>
<li>move backward</li>
<li>turn right 90 degrees (no move)</li>
<li>turn left 90 degrees (no move)</li>
</ol>

The starting direction of the object is 0, i.e up or "North".

## verbose (debug mode)

```
go run boardwalker.go <serie of digits> --verbose
```

## asciiart 
### See the board and how the marker moves 

Not tested on windows.

```
go run boardwalker.go <serie of digits> --asciiart
```

